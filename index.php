<?php 
session_start();
include 'private/connection.php';

if (isset($_GET['page'])) {
    $page   = $_GET['page'];
}
else{
    $page   = 'home';
}
?>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
</head>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">Tournament Maker</a>

<?php if(isset($_SESSION['role'])){ ?>
    <?php if($_SESSION['role'] == "admin"){ ?>

        <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=home">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=tournaments">Tournaments</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=manageaccount">Account management</a>
    </li>   
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=manageteams">Team management</a>
    </li>   
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=logout">Logout</a>
    </li>
  </ul>

<?php } ?>

<?php if($_SESSION['role'] == "referee"){ ?>

    <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=home">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=tournaments">Tournaments</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=changepass">Change password</a>
    </li>   
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=logout">Logout</a>
    </li>
  </ul>

<?php } ?>

<?php } else { ?>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=home">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=login">Login</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=tournamentsguest">Tournaments</a>
    </li>
  </ul>
<?php } ?>
</nav>

<body>
<?php include 'includes/'.$page.'.inc.php'; ?>
</body>
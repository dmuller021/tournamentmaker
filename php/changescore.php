<?php 
include '../private/connection.php';
session_start();

$tour_id    = $_POST['tour_id'];
$team_1     = $_POST['team_1'];
$team_2     = $_POST['team_2'];
$score_1    = $_POST['score_1'];
$score_2    = $_POST['score_2'];
$match_id   = $_POST['match_id'];

if($score_1 == $score_2)
{
    $_SESSION['score_error'] == "teams cannot have the same score";
    header('location: ../index.php?page=overview&tour_id=');
}
// else if($score_1 && $score_2 != 3)
// {
//     $_SESSION['score_error'] == "team MUST have score of 3 points otherwise they cannot advance";
//     header('location: ../index.php?page=editscore');
    // }

else
{
$sql = "UPDATE bracket_table
SET score_1 = :score1,
score_2     = :score2,
active      = 0
WHERE id = :id";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':score1'    => $score_1,
    ':score2'    => $score_2,
    ':id'        => $match_id
));

$sql2 = "SELECT *
FROM bracket_table
WHERE w1 = :w1 OR w2 = :w2";

$stmt2 = $conn->prepare($sql2);
$stmt2->execute(array(
    ':w1'   => $match_id,
    ':w2'   => $match_id
));

$r = $stmt2->fetch();

    if($r['r'] == 0)
    {
    header('location: ../index.php?page=winner&tour_id='.$tour_id.'');
    }
    else
    {

        if($r['w1'] == $match_id)
        {
            if($score_1 >= $score_2)
            {
        $sql3 = "UPDATE bracket_table
        SET team_1 = :team1
        WHERE w1 = :prevmatch";
        $stmt3 = $conn->prepare($sql3);
        $stmt3->execute(array(
            ':team1'        => $team_1,
            ':prevmatch'    => $match_id
        ));
            } 
        else
        {
            $sql3 = "UPDATE bracket_table
        SET team_1 = :team2
        WHERE w1 = :prevmatch";
        $stmt3 = $conn->prepare($sql3);
        $stmt3->execute(array(
            ':team2'        => $team_2,
            ':prevmatch'    => $match_id
        ));
        }
        }
        else
        {
            if($score_1 >= $score_2)
            {
        $sql4 = "UPDATE bracket_table
        SET team_2 = :team1
        WHERE w2 = :prevmatch";
        $stmt4 = $conn->prepare($sql4);
        $stmt4->execute(array(
            ':team1'        => $team_1,
            ':prevmatch'    => $match_id
        ));
            }
        else
        {
            $sql4 = "UPDATE bracket_table
        SET team_2 = :team2
        WHERE w2 = :prevmatch";
        $stmt4 = $conn->prepare($sql4);
        $stmt4->execute(array(
            ':team2'        => $team_2,
            ':prevmatch'    => $match_id
        ));
        }
        }
        header('location: ../index.php?page=overview&tour_id='.$tour_id.'');
    }
}




?>
<?php
include '../private/connection.php';

if(isset($_POST['edit'])) {
    $id             = $_POST['id'];
    $password       = $_POST['pass'];
    $repeatedpass   = $_POST['pass_repeat'];

    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    if($password != $repeatedpass){
        header('location: ../index.php?page=changepass');
    }
    else if
    
    else{
    $hashedpw = password_hash($password, PASSWORD_DEFAULT);

    $sql = "UPDATE user_table
    SET password = :password
    WHERE id = :id";

    $sth = $conn->prepare($sql);
    $result = $sth->execute(array(
        ':password'   => $hashedpw,
        ':id'         => $id
    ));

        if ($result){
          echo 'Successfully edited';
          header('location: ../index.php?page=home');
        }
        else{
          echo 'Something went wrong with the connection';
          header('location: ../index.php?page=changepass');
        }
    
    }
}

<?php
include '../private/connection.php';

if(isset($_POST['Add']))
{

$firstname            = $_POST['firstname'];
$midname              = $_POST['initials'];
$lastname             = $_POST['lastname'];
$email                = $_POST['email'];
$password             = $_POST['pass'];

$hashed = password_hash($password, PASSWORD_DEFAULT);

$uppercase = preg_match('@[A-Z]@', $password);
$lowercase = preg_match('@[a-z]@', $password);
$number    = preg_match('@[0-9]@', $password);
$specialChars = preg_match('@[^\w]@', $password);

$sth = $conn->prepare('SELECT * FROM user_table WHERE email =  :email'); //1. prepare statement variable
$sth->bindParam(':email', $email); //2. Value of the WHERE statement
$sth->execute(); //3. executing sql command
$result = $sth->rowCount();

if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
    header('location: ../index.php?page=Registration');
}
else if($result > 0 ) {
    header('location: ../index.php?page=test');
}

else {

$sql = "INSERT INTO user_table (voornaam, tussenvoegsel, achternaam, email, password, role, active)

VALUES (:firstname, :midname, :lastname, :email, :password, :role, :active)"; //1. query

$sth = $conn->prepare($sql); //2. prepare statement variable
$result = $sth->execute(array(
    ':firstname'    => $firstname,
    ':midname'      => $midname,
    ':lastname'     => $lastname,
    ':email'        => $email,
    ':password'     => $hashed,
    ':role'         => 'referee',
    ':active'       => 1
    ));

if ($result){
    echo 'Successfully created';
    }
else{
    echo 'Something went wrong with the connection';
    }
}
}
header('location: ../index.php?page=manageaccount'); 

?>
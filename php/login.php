<?php 
session_start();
include '../private/connection.php';

$password   = $_POST['password'];
$email   = $_POST['email'];

$sql = 'SELECT *
FROM user_table
WHERE email = :email AND active = 1';

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':email'        => $email
));
$user = $stmt->fetch(PDO::FETCH_ASSOC);

if(password_verify($password, $user['password']) && $user['role'] == "referee")  { //checks if the inputed password matches with the hashed password
    $_SESSION['role'] = "referee";
    $_SESSION['login'] = "Logged in as referee";
    $_SESSION['id']  = $user['id'];
    header('location: ../index.php?page=home');
}
else if(password_verify($password, $user['password']) && $user['role'] == "admin")  { //checks if the inputed password matches with the hashed password
    $_SESSION['role'] = "admin";
    $_SESSION['login'] ="Logged in as admin";
    header('location: ../index.php?page=home');
}
else{
    $_SESSION['login'] = "username or password is incorrect";
    header('location: ../index.php?page=login');
}
// header('location: ../index.php?page=login');

// if($r['password'] == $hashedpw && $r['role'] == "referee"){
//     $_SESSION['role'] = "referee";
//     $_SESSION['login'] = "Logged in as referee";
// }
// else if($r['password'] == $password && $r['role'] == "admin"){
//     $_SESSION['role'] = "admin";
//     $_SESSION['login'] ="Logged in as admin";
// }
// else{
//     $_SESSION['login'] = "username or password is incorrect";
    
// }


?>

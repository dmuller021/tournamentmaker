<?php 
include '../private/connection.php';

$id         = $_POST["team_ID"];
$name       = $_POST["teamname"];
$initials   = $_POST["initials"];



if($_FILES['newfile']['tmp_name'] != null){
$logo  = base64_encode(file_get_contents($_FILES['newfile']['tmp_name']));

$sql = "UPDATE team_table 
SET name    = :name,
initials    = :initials,
logo        = :logo
WHERE id = :id";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':name'     => $name,
    ':initials' => $initials,
    ':logo'     => $logo,
    ':id'       => $id
));
}

else{
    $sql2 = "UPDATE team_table 
    SET name    = :name,
    initials    = :initials
    WHERE id = :id";
    
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute(array(
        ':name'     => $name,
        ':initials' => $initials,
        ':id'       => $id
    ));

}

header('location: ../index.php?page=manageteams');
?>
<?php 
include '../private/connection.php';

$id = $_POST['tournament_ID'];

$sql = "UPDATE tournament_table
        SET active = 0
        WHERE id = :id";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':id'       => $id
));
header('location: ../index.php?page=tournaments'); 

?>
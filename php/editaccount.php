<?php
include '../private/connection.php';

if(isset($_POST['edit'])){

$voornaam        = $_POST['firstname']; //informatie uit editacc.inc.php
$initials        = $_POST['initials'];
$lastname        = $_POST['lastname'];
$email           = $_POST['email'];
$password        = $_POST['pass'];
$userid          = $_POST['user_id'];

}

$hashedpw = password_hash($password, PASSWORD_DEFAULT);

$sql2 = "UPDATE user_table
SET voornaam    = :name,
tussenvoegsel   = :initials,
achternaam      = :lastname,
email           = :email,
password        = :password

WHERE id = :id"; //1. query

$sth2 = $conn->prepare($sql2); //2. prepare statement variable
$result = $sth2->execute(array(
    ':name'             => $voornaam,
    ':initials'         => $initials,
    ':lastname'         => $lastname,
    ':email'            => $email,
    ':password'         => $hashedpw,
    ':id'               => $userid
    ));

if ($result){
    echo 'Successfully edited';
    header('location: ../index.php?page=manageaccount');
    }
else{
    echo 'Something went wrong with the connection';
    header('location: ../index.php?page=editacc');
    }


?>
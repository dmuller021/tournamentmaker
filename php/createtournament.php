<?php
include '../private/connection.php';

    $tourney_name       = $_POST["tourneyname"]; //tournament name
    $teams              = $_POST["teams"]; // 8, 16, 32 teams
    $round              = count($teams) / 2; //round

    // print_r($teams);
    
    $sql = "INSERT INTO tournament_table (name, active)
    VALUES (:name, :active)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':name'     => $tourney_name,
        ':active'   => 1
    ));

    $tour_id = $conn->lastInsertId();

    shuffle($teams);
    

    // foreach($teams as $index=>$value)
    // {
    //     print_r($value);
    //     echo '<br/>';
    // }


    for($i = 0; $i < sizeof($teams); $i+=2)
    {
        $sql2 = "INSERT INTO bracket_table (team_1, team_2, score_1, score_2, tour_id, active, r)
        VALUES (:team_1, :team_2, :score1, :score2, :tournament_id, :active, :round)";   
        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute(array(
            ':team_1'             => $teams[$i],
            ':team_2'             => $teams[$i+1],
            ':score1'             => 0,
            ':score2'             => 0,
            ':tournament_id'      => $tour_id,
            ':active'             => 1,
            ':round'              => $round
        ));
    }
    
    $round = $round / 2;
    
    do{
        $sql3 = "SELECT id
        FROM bracket_table
        WHERE tour_id = :tour_id AND r = (:round * 2)";
        $stmt3 = $conn->prepare($sql3);
        $stmt3->execute(array(
            ':tour_id'  => $tour_id,
            ':round'    => $round
        ));
        $r = $stmt3->fetchAll(PDO::FETCH_ASSOC);

        for($i = 0; $i < $round*2; $i+=2)
        {
            $sql4 = "INSERT INTO bracket_table (w1, w2, score_1, score_2, r, tour_id, active)
            VALUES (:w1, :w2, :score1, :score2, :r, :tour_id, :active)";
            $stmt4 = $conn->prepare($sql4);
            $stmt4->execute(array(
            ':w1'       => $r[$i]['id'],
            ':w2'       => $r[$i+1]['id'],
            ':score1'   => 0,
            ':score2'   => 0,
            ':r'        => $round,
            ':tour_id'  => $tour_id,
            ':active'   => 1
            ));
        }
        $round = $round / 2;
    }
    while($round >= 1);

    

    

    

    header('location: ../index.php?page=tournaments');
?>
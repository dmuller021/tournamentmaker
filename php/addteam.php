<?php 
include '../private/connection.php';
session_start();

$name       = $_POST["teamname"];
$initials   = $_POST["initials"];
$logo       = base64_encode(file_get_contents($_FILES['file']['tmp_name']));

$sql = "SELECT name
FROM team_table
WHERE name = :name OR initials = :initials";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':name'         => $name,
    ':initials'     => $initials
));


if($stmt->rowCount() > 0) 
{
    $_SESSION['melding2'] = "Name or initials already exist";
    header('location: ../index.php?page=addteam');
}

else {
$sql2 = "INSERT INTO team_table (name, initials, logo, active)
VALUES (:name, :initials, :logo, :active)";

$stmt2 = $conn->prepare($sql2);
$stmt2->execute(array(
    ':name'     => $name,
    ':initials' => $initials,
    ':logo'     => $logo,
    ':active'   => 1
));

header('location: ../index.php?page=manageteams');
}
?>
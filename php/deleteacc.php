<?php 
include '../private/connection.php';

$id = $_POST['user_ID'];

$sql = "UPDATE user_table
        SET active = 0
        WHERE id = :id";
$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':id'       => $id
));
header('location: ../index.php?page=manageaccount'); 

?>
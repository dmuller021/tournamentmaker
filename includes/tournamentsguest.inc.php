<?php 

$sql = 'SELECT *
FROM tournament_table
WHERE active = 1';
$smt = $conn->prepare($sql);
$smt->execute();
?>

<head>
  <title>Table bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Values to createtournament V  !-->

  <table class="table table-striped">
  <thead>
      <tr>
        <th>Tournament:</th>
      </tr>

    </thead>
  <?php while($r = $smt->fetch(PDO::FETCH_ASSOC)) { ?>
    <tbody>
      <tr>
        <td><?php echo $r['name'] ?></td>
        <td>
        <?php 
        echo '<a href="index.php?page=overviewguest&tour_id='.$r['id'].' method="GET">                                                    
        <button class="btn btn-primary">View</button></a>';
        ?>  
        </td>
      </tr>
    </tbody>
    <?php } ?>
  </table>
</div>

<?php
$tour_id    = $_GET['tour_id'];

$sql = 'SELECT score_1,
               score_2,
               tour_id,
               tt.name AS team1_name,
               tt2.name AS team2_name,
               tt.logo AS team1_logo,
               tt2.logo AS team2_logo
FROM bracket_table bt
INNER JOIN team_table tt
ON bt.team_1 = tt.id

INNER JOIN team_table tt2
ON bt.team_2 = tt2.id

WHERE tour_id = :id AND r = 1';
$stmt = $conn->prepare($sql); 
$stmt->execute(array(
    ':id'   => $tour_id
));


$r = $stmt->fetch();
?>

<?php if($r['score_1'] >= $r['score_2']) { ?>

<p>We have a winner!</p>
<img id="l_img" src="data:image/png;base64,<?php echo $r['team1_logo'] ?>" width=100 height=100 alt="team photo"/>
<p><?php echo $r['team1_name'] ?></p>

<?php  } 

else if($r['score_2'] >= $r['score_1']){ ?>

<p>We have a winner!</p>
<img id="l_img" src="data:image/png;base64,<?php echo $r['team2_logo'] ?>" width=100 height=100 alt="team photo"/>
<p><?php echo $r['team2_name'] ?> </p>

<?php  } 

else { ?>
<p>There is no winner yet!</p>
<?php } ?>
 
<a href="index.php?page=tournamentsguest&tour_id=<?php echo $tour_id ?>" class="btn btn-primary">Return to tourney page</button></a> 

<?php 
include 'private/connection.php';

$sql = 'SELECT *
FROM user_table
WHERE role = "referee" AND active = 1';
$smt = $conn->prepare($sql);
$smt->execute();
?>

<head>
  <title>Table bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
  <h2>Accounts:</h2> 
  <a href = "index.php?page=addaccount">

  <input class="btn btn-success" type="submit" name="" value="Add">

  </a>      
  <table class="table table-striped">
  <thead>
      <tr>
        <th>Firstname:</th>
        <th>Initials:</th>
        <th>Lastname:</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
  <?php while($r = $smt->fetch(PDO::FETCH_ASSOC)) { ?>
    <tbody>
      <tr>
        <td><?php echo $r['voornaam'] ?></td>
        <td><?php echo $r['tussenvoegsel'] ?></td>
        <td><?php echo $r['achternaam'] ?></td>

        <td>
            <form action="index.php?page=editacc" method="POST">
                <input type="hidden" name="user_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-primary">Edit</button>
            </form>
        </td>

        <td>
            <form action="php/deleteacc.php" method="POST">
                <input type="hidden" name="user_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-danger">Delete</button>
            </form>
        </td>
      </tr>
    </tbody>
    <?php } ?>
  </table>
</div>

</body>
<?php
include 'private/connection.php';
?>

<form method="POST" action="php/login.php">

<?php

    if(isset($_SESSION['login'])) {
        echo '<p>'.$_SESSION['login'].'</p>'; //Session echo
    }
?>

  <div class="form-group">
    <label for="email">E-mail:</label>
    <input type="email" class="form-control" placeholder="E-mail" name="email" required>
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" placeholder="Password" name="password" required>
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
</form>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>

<h1>Edit account</h1>
    <p>Please change the information that is required.</p>
    <hr>

<?php 
    $sql = 'SELECT *
    FROM user_table
    WHERE id = :id';
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':id'   => $_POST['user_ID']
    ));
    $r = $stmt->fetch();

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
?>

<form action="php/editaccount.php" method="POST">
  <div class="container">

    <input type="hidden" name="user_id" value="<?php echo $r['id'] ?>">

    <label for="firstname"><b>First name:</b></label>
    <input type="text" placeholder="Enter Firstname" name="firstname" value="<?php echo $r['voornaam'] ?>" required>

    <label for="initials"><b>Initials:</b></label>
    <input type="text" placeholder="Enter initial(s)" name="initials" value="<?php echo $r['tussenvoegsel'] ?>"  required>

    <label for="lastname"><b>Last name:</b></label>
    <input type="text" placeholder="Enter Lastname" name="lastname" value="<?php echo $r['achternaam'] ?>"  required>
    <hr>

    <label for="email"><b>Email:</b></label>
    <input type="text" placeholder="Enter Email" name="email" value="<?php echo $r['email'] ?>"  required>

    <label for="psw"><b>Password:</b></label>
    <p> IMPORTANT!! notify the user for his/her new password before you click on "Edit"</p>
    <input type="text" placeholder="Enter Password" name="pass" value="<?php echo generateRandomString(); ?>"  required>
    <hr>

    <input class="registerbtn" type="submit" name="edit" value="Edit">
    </form>

  </div>
</body>
</html>
<?php 
include 'private/connection.php';

$sql = 'SELECT *
FROM team_table
WHERE active = 1';

$smt = $conn->prepare($sql);
$smt->execute();
?>

<head>
  <title>Table bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
  <h2>Teams:</h2> 
  <a href = "index.php?page=addteam">

  <input class="btn btn-success" type="submit" name="" value="Add">

  </a>      
  <table class="table table-striped">
  <thead>
      <tr>
        <th>Logo:</th>
        <th>Team:</th>
        <th>Initials:</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
  <?php while($r = $smt->fetch(PDO::FETCH_ASSOC)) { ?>
    <tbody>
      <tr>
        <td> <img src="data:image/png;base64, <?php echo $r['logo'] ?>" width=100 height=100></td>
        <td>                                  <?php echo $r['name']       ?></td>
        <td>                                  <?php echo $r['initials']   ?></td>

        <td>
            <form action="index.php?page=editteam" method="POST">
                <input type="hidden" name="team_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-primary">Edit</button>
            </form>
        </td>

        <td>
            <form action="index.php?page=manageplayers" method="POST">
                <input type="hidden" name="team_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-info">Players</button>
            </form>
        </td>

        <td>
            <form action="php/deleteteam.php" method="POST">
                <input type="hidden" name="team_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-danger">Delete</button>
            </form>
        </td>
      </tr>
    </tbody>
    <?php } ?>
  </table>
</div>

</body>
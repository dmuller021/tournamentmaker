<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>

<h1>Edit account</h1>
    <p>Please change the information that is required.</p>
    <hr>

<?php 
    $sql = 'SELECT *
    FROM team_table
    WHERE id = :id';
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':id'   => $_POST['team_ID']
    ));
    $r = $stmt->fetch();
?>

<form action="php/editteam.php" method="POST" enctype="multipart/form-data">
  <div class="container">

    <input type="hidden" name="team_ID" value="<?php echo $r['id'] ?>">

    <label for="teamname"><b>Team name:</b></label>
    <input type="text" placeholder="Enter teamname:" name="teamname" value="<?php echo $r['name'] ?>" required>

    <label for="initials"><b>Initials:</b></label>
    <input type="text" placeholder="Enter initial(s)" name="initials" value="<?php echo $r['initials'] ?>"  required>


    <label for="currentlogo"><b>Current logo:</b></label>
    <td> <img src="data:image/png;base64, <?php echo $r['logo'] ?>" width=100 height=100></td>
    <hr>

    <input type="file" name="newfile">

    <input class="registerbtn" type="submit" name="edit" value="Edit">
    </form>

  </div>
</body>
</html>
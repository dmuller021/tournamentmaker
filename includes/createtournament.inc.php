   <?php
    include 'private/connection.php';

    $referees = $_POST['amount_of_teams'] / 2 ;
   ?>

   <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>

</head>
<body>

<h1>Add team</h1>
    <p>Please fill in this form to add a team.</p>
    <hr>

<form action="php/createtournament.php" method="POST">
  <div class="container">

<label for="name"><b>Name:</b></label>
    <input type="text" placeholder="Enter tournament name:" name="tourneyname" value=""  required>
    

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <div class="form-group col-md-4">
      <label for="inputState">Teams:</label>
      <select name="teams[]" id="inputState" class="form-control multiple-select" multiple>
        <?php 
        $sql = 'SELECT *
        FROM team_table
        WHERE active = 1';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
          foreach($stmt as $row)
          {
             ?>
              <option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
             <?php
          }
        }
        ?>
        </select>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
        $(".multiple-select").select2({
        maximumSelectionLength: <?php echo $_POST['amount_of_teams']; ?>
        });
        </script>

<div class="form-group col-md-4">
      <label for="inputState">Teams:</label>
      <select name="referees[]" id="inputState" class="form-control multiple-select" multiple>
        <?php 
        $sql2 = 'SELECT voornaam, tussenvoegsel, achternaam
        FROM user_table
        WHERE active = 1 AND role = referee';
        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();

        $r2 = $stmt2->fetch();

        $wholename = $r['voornaam'] . $r['tussenvoegsel'] . $r['achternaam'];

        if($stmt2->rowCount() > 0)
        {
          foreach($stmt2 as $row2)
          {
             ?>
              <option value="<?php echo $row2['id'] ?>"><?php echo $wholename ?></option>
             <?php
          }
        }
        ?>
        </select>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
        $(".multiple-select").select2({
        maximumSelectionLength: <?php echo $referees; ?>
        });
        </script>
  </div>
  <div class="container">
            <label for="">Click to add</label><br>
            <input class="registerbtn" type="submit" name="add_tournament" value="Add">
  </div>
  </form>
</body>
</html>

        <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
        $(".multiple-select").select2({
        maximumSelectionLength:  echo $_POST['amount_of_teams']; ?>
        });
        </script> -->
      
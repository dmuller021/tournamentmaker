<?php 

$tour_id   = $_POST['tour_id'];
$team_1    = $_POST["team_1"];
$team_2    = $_POST["team_2"];

$sql = "SELECT  tt.id AS tt_id,
                tt2.id AS tt2_id,
                bt.id AS match_id,
               tt.name AS team1_name,
               tt2.name AS team2_name,
               tt.logo AS team1_logo,
               tt2.logo AS team2_logo,
               score_1,
               score_2,
               tour_id,
               bt.active    
FROM bracket_table bt
INNER JOIN team_table tt
ON bt.team_1 = tt.id

INNER JOIN team_table tt2
ON bt.team_2 = tt2.id

WHERE tt.id = :id AND tt2.id = :id2 AND tour_id = :tour_id";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':id'       => $team_1,
    ':id2'      => $team_2,
    ':tour_id'  => $tour_id
)); 
?>

<h2 class="txtalign2">Edit score</h2>
<hr>
<?php if(isset($_SESSION['score_error'])){
    echo '<p>'.$_SESSION['score_error'].'</p>';
} ?>
<?php while($r = $stmt->fetch()) { ?>
<div class="mt-5">
<div class="widen justify-content-center ">
    <div class="d-flex row">
        <div class="align-content-center col-md-6">
                <div class="rating-box mb-3">
                    <div class="border rounded">
                        <div class="text-center score py-2">
                        <form action= "php/changescore.php" method= POST>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="d-flex mt-0 p-2"><img id="l_img" src="data:image/png;base64,<?php echo $r['team1_logo'] ?>" width=100 height=100 alt="team photo"/></div>
                                    <input type="textbox" name="score_1" required> 
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex mt-0 p-2"><img id="l_img" src="data:image/png;base64,<?php echo $r['team2_logo'] ?>" width=100 height=100 alt="team photo"/></div>
                                    <input type="textbox" name="score_2" required>
                                </div>
                            </div>
                            <div>
                            <input type="hidden" name="match_id" value="<?php echo $r['match_id'] ?>">
                            <input type="hidden" name="team_1" value="<?php echo $r['tt_id'] ?>">
                            <input type="hidden" name="team_2" value="<?php echo $r['tt2_id'] ?>">
                            <input type="hidden" name="tour_id" value="<?php echo $r['tour_id'] ?>">
                            <button class="btn btn-primary">Change score</button>
                            </div>
                        </form>
                            <div><span>Match standings</span></div>
                            <!-- print score -->
                            <div class="rating-out"><h2 style="display:inline-block;" class="get-rating"><?= $r['score_1'] ?> :</h2>
                            <h2 style="display:inline-block"><?= $r['score_2'] ?></h2>  

                            <!-- checks if both the scores are equal to 0  -->
 <?php           if ($r['score_1'] || $r['score_2'] != 0) { ?>
                    <p class="text-success">This match is finished. </p>
<?php          } 
                
                 
                else { ?>
                    <p class="text-danger">This match has yet to be played.</p>
<?php          } ?>       


                            <hr>
                            <!-- print football teams names -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <b class="d-flex mt-0 p-2 justify-content-center "><?= $r['team1_name'] ?></b>
                                </div>
                                <div class="col-lg-6">
                                    <b class="d-flex mt-0 p-2 justify-content-center "><?= $r['team2_name'] ?></b>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php 

$sql = 'SELECT *
FROM tournament_table
WHERE active = 1';
$smt = $conn->prepare($sql);
$smt->execute();
?>

<head>
  <title>Table bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Values to createtournament V  !-->
<form action="index.php?page=createtournament" method="POST">
<div class="container">
  <h2>Tournaments:</h2> 

  <p>Choose amount of teams:
  <select name="amount_of_teams">
  <option value="8">8</option>
  <option value="16">16</option>
  <option value="32">32</option>
</select>
</p>  
<input class="btn btn-success" type="submit" name="" value="Add">
</form>

  <table class="table table-striped">
  <thead>
      <tr>
        <th>Tournament:</th>
      </tr>

    </thead>
  <?php while($r = $smt->fetch(PDO::FETCH_ASSOC)) { ?>
    <tbody>
      <tr>
        <td><?php echo $r['name'] ?></td>
        <td>
            <?php 
               echo '<a href="index.php?page=overview&tour_id='.$r['id'].'" class="btn btn-primary">View</a>';
               ?>  
        </td>

        <td>
            <form action="php/deletetourney.php" method="POST">
                <input type="hidden" name="tournament_ID" value="<?php echo $r['id'] ?>">                                                     
                <button class="btn btn-danger">Delete</button>
            </form>
        </td>
      </tr>
    </tbody>
    <?php } ?>
  </table>
</div>

</body>